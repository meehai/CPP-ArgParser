#include <ArgParser.hpp>
#include <iostream>

#define EXAMPLE 2

void example1(ArgParser &parser) {
	parser.setUsage("Usage: ./argparser-test <x> <abcd=\"message\"> [abc=[0,99]]");
	parser.addArgument(Argument("x"), ArgType::MANDATORY);
	parser.addArgument(ValuedArgument<int>("abc", "", [](int x) { return x < 100 && x >= 0; }));
	parser.addArgument(ValuedArgument<std::string>(std::string("abcd")), ArgType::MANDATORY);

	parser.parseArguments();

	std::cout<<parser.toString()<<"\n";
	/* This also checks if "abc" is defined above */
	if (parser.isSet("abc"))
		std::cout<<"abc is defined and set. Value = "<<parser.getValue<int>("abc")<<"\n";
	else {
		/* using getValue here will throw an exception saying "abc" is not set. */
		std::cout<<"abc is undefined\n";
	}

	try {
		auto x = parser.getValue<std::string>("abc");
	} catch (std::runtime_error &) {
		std::cout<<"This should fail becuase 'abc' is of type int\n";
	}

	/* mandatory argument, therefore guaranteed to be defined */
	std::cout<<"abcd = "<<parser.getValue<std::string>("abcd")<<"\n"; 
}

void example2(ArgParser &parser) {
	parser.setUsage("Usage: ./argparser-test <x=[0,100]>, <y=[-50,50]>, <type={\"sum\", \"product\", \"diff\"}> ");
	parser.addArgument(ValuedArgument<int>("x", "", [](int x) {return x >=0 && x<= 100; }), ArgType::MANDATORY);
	parser.addArgument(ValuedArgument<float>("y", "", [](float x) {return x >=-50 && x<= 50; }), ArgType::MANDATORY);
	parser.addArgument(ValuedArgument<std::string>("type", "", [](std::string x) {return x == "sum" || x == "product" 
		|| x == "diff";}), ArgType::MANDATORY);
	parser.parseArguments();

	if(parser.getValue<std::string>("type") == "sum")
		std::cout << "Sum: " << parser.getValue<int>("x") + parser.getValue<float>("y") << "\n";
	else if(parser.getValue<std::string>("type") == "product")
		std::cout << "Product: " << parser.getValue<int>("x") * parser.getValue<float>("y") << "\n";
	else
		std::cout << "Diff: " << parser.getValue<int>("x") - parser.getValue<float>("y") << "\n";
}

int main(int argc, char *argv[]) {
	ArgParser parser(argc, argv);

#if EXAMPLE == 1
	example1(parser);
#elif EXAMPLE == 2
	example2(parser);
#else
#error "Example 1 or 2 must be defined at compile time"
#endif

	return 0;
}