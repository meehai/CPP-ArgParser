/* Argument.h - Arg Parser - Pirvu Mihai Cristian */
#ifndef ARGUMENT_H
#define ARGUMENT_H

#include <algorithm>
#include <memory>
#include <string>
#include <stdexcept>

class Argument {
public:
	Argument() = delete;
	Argument(const std::string &, const std::string & = "");
	const std::string &getName() const;
	const std::string &getDescription() const;
	virtual std::string toString();
	virtual void set(bool = true);
	virtual bool isSet();
	virtual void setValue(std::string &);
	~Argument() {};

protected:
	const std::string name, description;
	bool isSetFlag = false;
};

#endif
